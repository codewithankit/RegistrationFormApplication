package com.example.registrationformapplication.viewModal

import androidx.lifecycle.ViewModel
import com.example.registrationformapplication.model.Student
import com.example.registrationformapplication.obserable.FormFieldObserable
import com.example.registrationformapplication.repository.SqliteDBRepository

class FormActivityViewModel(private val repository: SqliteDBRepository) : ViewModel() {
    //----------------------------------------------------------------------------------------------
    private var firstName:String=""
    private var lastName:String=""
    private var mobileNumber:String=""
    private var altMobileNumber:String=""
    private var email:String=""
    private var _gender:String=""
    private var dob:String=""
    private var address:String=""
    private lateinit var languages:String

    //----------------------------------------------------------------------------------------------
    val formfieldobserable : FormFieldObserable = FormFieldObserable()


    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------------
    fun datavalidation(gender : String, arrayList : ArrayList<String>) {
        firstName= formfieldobserable.fnameform
        lastName= formfieldobserable.lnameform
        mobileNumber= formfieldobserable.mobilenumberform
        altMobileNumber= formfieldobserable.altnumberform
        address= formfieldobserable.addressform
        email= formfieldobserable.emailform
        dob=formfieldobserable.dobform
        this._gender = gender
        languages= arrayList.toString()

       repository.createData(firstName,lastName,mobileNumber,altMobileNumber,dob,email,_gender,address,languages)




    }
    //----------------------------------------------------------------------------------------------

    fun getAllData(): ArrayList<Student>{
       return repository.getAllData()
    }


    fun deleteOneData(rowId:Int){
        repository.deleteSingleRecord(rowId)
    }
}
//--------------------------------------------------------------------------------------------------