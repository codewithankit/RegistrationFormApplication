package com.example.registrationformapplication.activity

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.R
import com.example.registrationformapplication.adapter.MyListViewAdapter
import com.example.registrationformapplication.databinding.ActivityDataListViewFormBinding
import com.example.registrationformapplication.factory.FormActivityViewModelFactory
import com.example.registrationformapplication.repository.SqliteDBRepository
import com.example.registrationformapplication.viewModal.FormActivityViewModel

class DataListViewFormActivity : AppCompatActivity(), AdapterView.OnItemClickListener {
    private lateinit var binding : ActivityDataListViewFormBinding
    private lateinit var viewModel:FormActivityViewModel
    private lateinit var factory:FormActivityViewModelFactory
    private var rowId = 0
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_data_list_view_form)
        factory=FormActivityViewModelFactory(SqliteDBRepository(this))
        viewModel = ViewModelProvider(this,factory)[FormActivityViewModel::class.java]
        setListData()
        binding.listView.setOnItemClickListener(this)
    }

   private fun setListData(){
        val listofStudents=viewModel.getAllData()
        val myListAdapter=MyListViewAdapter(listofStudents)
        binding.listView.adapter=myListAdapter
    }

    override fun onItemClick(parent : AdapterView<*>?, view : View?, position : Int, id : Long) {
        TODO("Not yet implemented")
    }


}