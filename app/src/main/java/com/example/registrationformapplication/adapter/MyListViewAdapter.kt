package com.example.registrationformapplication.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.LayoutItemsBinding
import com.example.registrationformapplication.model.Student

class MyListViewAdapter(private val listOfStudents:ArrayList<Student>):BaseAdapter() {
     override fun getCount() : Int {
       return listOfStudents.size
     }

     override fun getItem(position : Int) : Any {
         return listOfStudents[position]
     }

     override fun getItemId(position : Int) : Long {
         return listOfStudents[position].hashCode().toLong()
     }

     @SuppressLint("ViewHolder")
     override fun getView(position : Int, convertView : View?, parent : ViewGroup?) : View {
         var view=convertView
         view=LayoutInflater.from(parent!!.context).inflate(R.layout.layout_items,parent,false)
         val binding=DataBindingUtil.bind<LayoutItemsBinding>(/* root = */ view)
         binding!!.firstNameLayoutItem.text=listOfStudents[position].fName
         binding.lastNameLayoutItem.text=listOfStudents[position].lName
         binding.phoneNumberLayoutItem.text=listOfStudents[position].phone
         return view
     }
 }