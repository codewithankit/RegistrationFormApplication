package com.example.registrationformapplication.obserable

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

class SignUpFieldObserable : BaseObservable() {

    @get:Bindable
    var mobileNumberLogin : String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.mobileNumberLogin)
        }

    @get:Bindable
    var fnamesignup : String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.fnamesignup)
        }

    @get:Bindable
    var lnamesignup : String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.lnamesignup)
        }

    @get:Bindable
    var mobileNumberSignUp : String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.mobileNumberSignUp)
        }


}