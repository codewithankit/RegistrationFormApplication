package com.example.registrationformapplication.model

data class Student(
    var srNo: String,
    var fName: String,
    var lName: String,
    var phone: String,
    var altphone:String,
    var dob:String,
    var email:String,
    var gender:String,
    var address:String,
    var languages :String
    )